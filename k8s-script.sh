
#!/bin/bash 

dt=`date +"%Y%m%d-%H%M%S"`; echo $dt; d2=`date +"%Y%m%d-%T"`; echo $d2
alias k='kubectl'

echo "Building the base ENVs..." 
DIR=${PWD}/yml/$dt
	echo ${DIR}
ALLNSF=${DIR}/allns.log
	echo ${ALLNSF}
KGETNSF=${DIR}/kget-ns.log
	echo ${KGETNSF}


echo "Pause for 5 seconds...."
mkdir -p ${DIR}
kubectl get all --all-namespaces > ${ALLNSF}

kgetns=`kubectl get ns | egrep -v 'heptio-ark|kube-public|kube-system|NAME' | awk '{print $1}'`
kubectl get ns | egrep -v 'heptio-ark|kube-public|kube-system|NAME' | awk '{print $1}' > ${KGETNSF}

kubectl api-resources > ${DIR}/options 2>&1

for ns in `cat ${KGETNSF}`
do echo "## PHASE1: INFO:: This is for NameSpace: ${ns}... ##############"
  kubens ${ns}
  # for i in certificatesigningrequests clusterrolebindings clusterroles componentstatuses configmaps controllerrevisions daemonsets deployments endpoints events horizontalpodautoscalers ingresses jobs limitranges networkpolicies nodes persistentvolumeclaims persistentvolumes poddisruptionbudgets podpreset pods podsecuritypolicies podtemplates replicasets replicationcontrollers resourcequotas rolebindings roles secrets serviceaccounts services statefulsets storageclasses 
  FOYO=${DIR}/FOYO.all
  FOYO2=${DIR}/FOYO2.all
  echo "kongingress" >> ${FOYO}
  #cat /opt/tfscripts/post/fino | grep "*" | grep -v "ev" | cut -d* -f2  | tr -d "[:blank:]" | awk '{print $1}'|  cut -d"(" -f1 | grep -v all >> ${FOYO}
  #cat ${DIR}/options | grep "*" | grep -v "ev" | cut -d* -f2  | tr -d "[:blank:]" | awk '{print $1}'|  cut -d"(" -f1 | grep -v all >> ${FOYO}
  cat ${DIR}/options | grep true | awk '{print $1}' >> ${FOYO}
  cat ${FOYO} | sort -u >> ${FOYO2}
  for fino in `cat ${FOYO2}`
   do echo "## PHASE2: This is NameSpace=${ns} --> obtaining kubectl for=${fino} .. ##########"
    dir1=""
    dir1="${DIR}/${ns}/${fino}"
    all1=""
    all1="${dir1}/all-${fino}.txt"
     mkdir -p ${dir1}
	echo " HASAN "
     kubectl get ${fino} | grep -v NAME | awk '{print $1}' > ${all1}
     for finock in `cat ${all1} | grep -v "^$"`
      do echo "## PHASE 3: ---> this is for $finock ..."
       all2=""
       all2="${dir1}/$finock.yaml"
       #kubectl get ${fino} $finock -o yaml | egrep -v 'creationTimestamp:|generation: 1|resourceVersion:|selfLink:|uid:' | awk '/status/ {exit} {print}' > ${all2}
       #kubectl get ${fino} $finock -o yaml | egrep -v 'creationTimestamp:|generation: 1|resourceVersion:|selfLink:|uid:' > ${all2}
       kubectl get ${fino} $finock -o yaml > ${all2}
      done
   done
done

FOYO3=${DIR}/FOYO3.all
cat ${DIR}/options | grep false | awk '{print $1}' >> ${FOYO3}

for i in `cat ${FOYO3}`;
  do echo "## PHASE 4: Starting - $i - Checks... "
    dir2=""
    dir2="${DIR}/${i}"
    all3=""
    all3="${dir2}/all-${i}.txt"
    mkdir -p ${dir2}
    kubectl get ${i} | grep -v NAME | awk '{print $1}' > ${all3}
    for globalcheck in `cat ${all3} | grep -v "^$"`
      do echo "## PHASE 5: ---> this is for GLOBAL: $globalcheck..."
        all4=""
        all4="${dir2}/$globalcheck.yaml"
        #kubectl get $i ${globalcheck} -o yaml | egrep -v 'creationTimestamp:|generation: 1|resourceVersion:|selfLink:|uid:' | awk '/status/ {exit} {print}' > ${all4}
        kubectl get $i ${globalcheck} -o yaml > ${all4}
      done
  done

echo " <<<<< ----------------- ALL EXTRACTS COMPLETED ------------------ >>>>> "
echo $DIR
